package com.crimsonrpg.magicrpg;

import org.bukkit.Location;

public class StreamData {
	private Location loc;

	private boolean pastTurningPoint;

	public StreamData(Location loc, boolean pastTurningPoint) {
		this.loc = loc;
		this.pastTurningPoint = pastTurningPoint;
	}

	public boolean isPastTurningPoint() {
		return pastTurningPoint;
	}

	public Location getLocation() {
		return loc;
	}

	public void setPastTurningPoint(boolean b) {
		this.pastTurningPoint = b;
	}

	public String toString() {
		return loc.getX() + "   " + loc.getY() + "   " + loc.getZ() + "   " + pastTurningPoint;
	}

}