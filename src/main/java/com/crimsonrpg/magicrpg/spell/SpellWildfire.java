package com.crimsonrpg.magicrpg.spell;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
@SpellInfo(id = "wildfire",
name = "Wildfire")
public class SpellWildfire implements Spell {

    public void cast(Player caster) {
        Location epicenter = caster.getLocation();
        World world = epicenter.getWorld();
        int xmin = epicenter.getBlockX() - 10;
        int zmin = epicenter.getBlockZ() - 10;
        int xmax = epicenter.getBlockX() + 10;
        int zmax = epicenter.getBlockZ() + 10;

        for (int i = xmin; i <= xmax; i++) {
            for (int k = zmin; k <= zmax; k++) {
                Block highestBlockAt = world.getHighestBlockAt(i, k);
                Location loco = highestBlockAt.getLocation();
                if (loco.distanceSquared(epicenter) <= 100 && loco.getBlockY() < 128) {
                    highestBlockAt.getRelative(BlockFace.UP).setType(Material.FIRE);
                }
            }
        }
    }
}
