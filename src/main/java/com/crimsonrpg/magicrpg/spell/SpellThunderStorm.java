/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "thunderstorm",
name = "ThunderStorm")
public class SpellThunderStorm implements Spell {

    public Map<String, Integer> taskID = new HashMap<String, Integer>();
    private MagicRPG mr;

    public SpellThunderStorm(MagicRPG mr) {
        this.mr = mr;
    }

    public void cast(final Player caster) {
        final Random rand = new Random();
        final Location playerLoc = caster.getLocation();


        final int stormTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(mr, new Runnable() {

            int i = 0;

            public void run() {

                i++;
                if (i == 10) {
                    Bukkit.getScheduler().cancelTask(taskID.get(caster.getName()));
                }
                Location loc = new Location(caster.getWorld(), playerLoc.getBlockX() + rand.nextInt(10), playerLoc.getBlockY() - rand.nextInt(5), playerLoc.getBlockZ() + rand.nextInt(10));
                caster.getWorld().strikeLightning(loc);
                caster.getWorld().strikeLightning(loc);
            }
        }, 0L, 20L);
        taskID.put(caster.getName(), stormTask);

    }
}
