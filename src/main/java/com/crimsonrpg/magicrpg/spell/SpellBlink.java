/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "blink",
name = "Blink")
public class SpellBlink implements Spell {
	public void cast(Player caster) {
		if (caster.getTargetBlock(null, 50).getLocation().distance(caster.getLocation()) > 15) {
			return;
		}
		caster.teleport(caster.getTargetBlock(null, 50).getLocation());
	}

}
