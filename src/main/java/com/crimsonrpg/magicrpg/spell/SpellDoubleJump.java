/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "doublejump",
name = "Double Jump")
public class SpellDoubleJump implements Spell {

    public void cast(Player caster) {
        caster.setVelocity(new Vector(0, 1, 0));
    }
}
