/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
@SpellInfo(id = "tsunami",
name = "Tsunami")
public class SpellTsunami implements Spell {
	public void cast(Player caster) {
		Location epicenter = caster.getLocation();
		int xmin = epicenter.getBlockX() - 10;
		int ymin = epicenter.getBlockY() - 10;
		int zmin = epicenter.getBlockZ() - 10;
		int xmax = epicenter.getBlockX() + 10;
		int ymax = epicenter.getBlockY() + 10;
		int zmax = epicenter.getBlockZ() + 10;

		for (int i = xmin; i <= xmax; i++) {
			for (int j = ymin; j <= ymax; j++) {
				for (int k = zmin; k <= zmax; k++) {
					Location pos = new Location(epicenter.getWorld(), i, j, k);
					if (pos.distanceSquared(epicenter) <= 100 && pos.getBlock().getType().equals(Material.AIR)) {
						pos.getBlock().setType(Material.WATER);
					}
				}
			}
		}

	}

}
