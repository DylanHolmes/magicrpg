/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.ComplexLivingEntity;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityListener;
import org.bukkit.event.entity.EntityTargetEvent;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "enderdragon",
name = "Enderdragon")
public class SpellEnderDragon extends EntityListener implements Spell {

    MagicRPG mr;

    @SuppressWarnings("LeakingThisInConstructor")
    public SpellEnderDragon(MagicRPG mr) {
        this.mr = mr;
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_TARGET, this, Priority.Normal, mr);
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_DAMAGE, this, Priority.Normal, mr);
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_DEATH, this, Priority.Normal, mr);
    }
    private Map<Player, ComplexLivingEntity> enderDragonOwnerShip = new HashMap<Player, ComplexLivingEntity>();
    private Map<ComplexLivingEntity, Player> playerOwnerShip = new HashMap<ComplexLivingEntity, Player>();
    private Map<ComplexLivingEntity, Entity> target = new HashMap<ComplexLivingEntity, Entity>();

    public void cast(final Player caster) {
        spawnEnder(caster);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(mr, new Runnable() {

            public void run() {
                followPlayer(caster);
                Bukkit.getScheduler().scheduleSyncDelayedTask(mr, new Runnable() {

                    public void run() {
                        deSpawnEnder(caster);
                    }
                }, 2400L);
            }
        }, 0L, 40L);
    }

    private void followPlayer(Player caster) {
        ComplexLivingEntity mob = enderDragonOwnerShip.get(caster);
        Location loc = caster.getLocation();
        if (loc.distance(mob.getLocation()) > 25) {
            Location playerLoc = new Location(caster.getWorld(), loc.getBlockX() - 2, loc.getBlockY(), loc.getBlockZ());
            mob.teleport(playerLoc);
        }
    }

    private void spawnEnder(Player caster) {
        ComplexLivingEntity enderDragon = (ComplexLivingEntity) caster.getWorld().spawnCreature(caster.getLocation(), CreatureType.ENDER_DRAGON);
        enderDragonOwnerShip.put(caster, enderDragon);
        playerOwnerShip.put(enderDragon, caster);
                enderDragon.setPassenger(caster);
    }

    private void deSpawnEnder(Player caster) {
        ComplexLivingEntity enderDragon = this.enderDragonOwnerShip.get(caster);
        enderDragon.remove();
    }

    @Override
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        final EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (!(e.getDamager() instanceof Player)) {
            return;
        }
        if (enderDragonOwnerShip.containsKey((Player) e.getDamager())) {
            if (e.getDamager() instanceof Player) {
                final ComplexLivingEntity mob = enderDragonOwnerShip.get((Player) e.getDamager());
                target.put(mob, e.getEntity());
                Bukkit.getScheduler().scheduleSyncRepeatingTask(mr, new Runnable() {

                    public void run() {
                        if (e.getEntity().isDead()) {
                            target.remove(mob);
                            return;
                        }
                        mob.damage(3, e.getEntity());
                    }
                }, 0, 40L);
            }
        }
    }

    @Override
    public void onEntityTarget(EntityTargetEvent event) {

        if (event.getTarget() instanceof Player && event.getTarget() == playerOwnerShip.get((ComplexLivingEntity) event.getEntity())) {
            event.setCancelled(true);
        }
    }
}