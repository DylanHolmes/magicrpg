/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "explosion",
name = "Explosion")
public class SpellExplosion implements Spell {

    public void cast(Player caster) {
        Location loc = caster.getLocation();
        if (caster.getTargetBlock(null, 50).getLocation().distance(loc) > 15) {
            return;
        }
        caster.getWorld().createExplosion(caster.getTargetBlock(null, 50).getLocation(), 1f);
    }
}
