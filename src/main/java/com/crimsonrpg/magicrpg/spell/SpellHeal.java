/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "heal",
name = "Heal")
public class SpellHeal implements Spell {
	public void cast(Player caster) {
		caster.setHealth(caster.getHealth() + 6);
	}

}
