/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;

/**
 *
 * @author simplyianm
 */
@SpellInfo(id = "tornado",
name = "Tornado")
public class SpellTornado implements Spell {
	private MagicRPG rpg;

	public SpellTornado(MagicRPG rpg) {
		this.rpg = rpg;
	}

	public void cast(Player caster) {
		Tornado tornado = new Tornado(caster.getLocation(), 10, 20);
		tornado.runFor(1000);
	}

	private static double lengthSq(double x, double z) {
		return (x * x) + (z * z);
	}

	public class Tornado {
		private Location center;

		private int bottomRadius;

		private int topRadius;

		private int task = -1;

		private int iterations = 0;

		private int maxIterations = 0;

		public Tornado(Location center, int bottomRadius, int topRadius) {
			this.center = center;
			this.bottomRadius = bottomRadius;
			this.topRadius = topRadius;
		}

		public void runFor(int iterations) {
			maxIterations = iterations;
			start();
		}

		public void start() {
		}

	}

}
