/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;
import java.util.HashMap;
import java.util.Map;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Creature;
import org.bukkit.entity.CreatureType;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityListener;
import org.bukkit.event.entity.EntityTargetEvent;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "enderman",
name = "Enderman")
public class SpellEnderman extends EntityListener implements Spell {

    MagicRPG mr;

    @SuppressWarnings("LeakingThisInConstructor")
    public SpellEnderman(MagicRPG mr) {
        this.mr = mr;
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_TARGET, this, Priority.Normal, mr);
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_DAMAGE, this, Priority.Normal, mr);
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_DEATH, this, Priority.Normal, mr);
    }
    private Map<Player, Creature> enderMenOwnerShip = new HashMap<Player, Creature>();
    private Map<Creature, Player> playerOwnerShip = new HashMap<Creature, Player>();
    private Map<Creature, Entity> target = new HashMap<Creature, Entity>();

    public void cast(final Player caster) {
        spawnEnder(caster);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(mr, new Runnable() {

            public void run() {
                followPlayer(caster);
                Bukkit.getScheduler().scheduleSyncDelayedTask(mr, new Runnable() {

                    public void run() {
                        deSpawnEnder(caster);
                    }
                }, 2400L);
            }
        }, 0L, 40L);
    }

    private void followPlayer(Player caster) {
        Creature mob = enderMenOwnerShip.get(caster);

        Location loc = caster.getLocation();
        Location playerLoc = new Location(caster.getWorld(), loc.getBlockX() - 2, loc.getBlockY(), loc.getBlockZ());
        mob.teleport(playerLoc);
    }

    private void spawnEnder(Player caster) {
        Creature endermen = (Creature) caster.getWorld().spawnCreature(caster.getLocation(), CreatureType.ENDERMAN);
        enderMenOwnerShip.put(caster, endermen);
        playerOwnerShip.put(endermen, caster);
    }

    private void deSpawnEnder(Player caster) {
        Creature endermen = this.enderMenOwnerShip.get(caster);
        endermen.remove();
    }

    @Override
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }

        final EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (!(e.getDamager() instanceof Player)) {
            return;
        }
        if (enderMenOwnerShip.containsKey((Player) e.getDamager())) {
            if (e.getDamager() instanceof Player) {
                final Creature mob = enderMenOwnerShip.get((Player) e.getDamager());
                target.put(mob, e.getEntity());
                Bukkit.getScheduler().scheduleSyncRepeatingTask(mr, new Runnable() {

                    public void run() {
                        if (e.getEntity().isDead()) {
                            target.remove(mob);
                            return;
                        }
                        mob.damage(3, e.getEntity());
                    }
                }, 0, 40L);
            }
        }
    }

    @Override
    public void onEntityTarget(EntityTargetEvent event) {

        if (event.getTarget() instanceof Player && event.getTarget() == playerOwnerShip.get((Creature) event.getEntity())) {
            event.setCancelled(true);
        }
    }
}
