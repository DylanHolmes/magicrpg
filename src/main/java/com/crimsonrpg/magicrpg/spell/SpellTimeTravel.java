/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.entity.Player;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "timetravel",
name = "Time Travel")
public class SpellTimeTravel implements Spell {

    public void cast(Player caster) {
        if (caster.getWorld().getTime() > 12000) {
            caster.getWorld().setTime(6000L);
        } else {
            caster.getWorld().setTime(18000);
        }
    }
}
