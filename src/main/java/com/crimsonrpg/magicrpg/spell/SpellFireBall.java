/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Priority;
import org.bukkit.event.Event.Type;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityListener;

/**
 *
 * @author Dylan
 */
@SpellInfo(id = "fireball",
name = "FireBall")
public class SpellFireBall extends EntityListener implements Spell {

    ArrayList FireBall = new ArrayList();
    MagicRPG mr;

    @SuppressWarnings("LeakingThisInConstructor")
    public SpellFireBall(MagicRPG mr) {
        this.mr = mr;
        Bukkit.getPluginManager().registerEvent(Type.ENTITY_EXPLODE, this, Priority.Normal, mr);

    }

    public void cast(Player caster) {
        Location loc = caster.getLocation();
        if (loc.distance(caster.getTargetBlock(null, 20).getLocation()) > 20) {
            caster.sendMessage("You charge the spell, but you are not strong enough to cast that far.");
            return;
        }
        spawnFireBall(caster);
    }

    private void spawnFireBall(Player player) {
        Fireball fB = player.getWorld().spawn(player.getTargetBlock(null, 20).getLocation(), Fireball.class);
        FireBall.add(fB);
        Location loc = new Location(fB.getWorld(), fB.getLocation().getBlockX() - 2, fB.getLocation().getBlockY(), fB.getLocation().getBlockZ());
        Block highBlock = fB.getWorld().getHighestBlockAt(loc);
        fB.setVelocity(player.getVelocity());
        highBlock.getRelative(BlockFace.UP).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.NORTH).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.SOUTH).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.WEST).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.EAST).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.EAST_NORTH_EAST).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.WEST_NORTH_WEST).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.SELF).setType(Material.FIRE);
        highBlock.getRelative(BlockFace.WEST_SOUTH_WEST).setType(Material.FIRE);
        if (fB.getLocation().distance(player.getLocation()) > 30) {
            fB.remove();
        }
    }

    @Override
    public void onEntityExplode(EntityExplodeEvent event) {
        if (FireBall.contains(event.getEntity())) {
            FireBall.remove(event.getEntity());
            event.setCancelled(true);
        }
    }
}
