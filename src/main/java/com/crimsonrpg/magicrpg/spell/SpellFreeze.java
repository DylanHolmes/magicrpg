/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg.spell;

import com.crimsonrpg.magicrpg.MagicRPG;
import com.crimsonrpg.magicrpg.SpellInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
@SpellInfo(id = "freeze",
name = "Freeze")
public class SpellFreeze implements Spell {
	private MagicRPG magicrpg;

	public SpellFreeze(MagicRPG magicrpg) {
		this.magicrpg = magicrpg;
	}

	public void cast(Player caster) {
		final Location location = caster.getTargetBlock(null, 50).getLocation();
		final Location minBound = location.subtract(2, 2, 2);
		final Location maxBound = location.add(2, 2, 2);

		for (int i = minBound.getBlockX(); i <= maxBound.getBlockX(); i++) {
			for (int j = minBound.getBlockY(); j <= maxBound.getBlockY(); i++) {
				for (int k = minBound.getBlockZ(); k <= maxBound.getBlockZ(); i++) {
					Location l = new Location(location.getWorld(), i, j, k);
					if (l.getBlock().getType().equals(Material.AIR)) {
						l.getBlock().setType(Material.ICE);
					}
				}
			}
		}

		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(magicrpg, new Runnable() {
			public void run() {
				for (int i = minBound.getBlockX(); i <= maxBound.getBlockX(); i++) {
					for (int j = minBound.getBlockY(); j <= maxBound.getBlockY(); i++) {
						for (int k = minBound.getBlockZ(); k <= maxBound.getBlockZ(); i++) {
							Location l = new Location(location.getWorld(), i, j, k);
							if (l.getBlock().getType().equals(Material.ICE)) {
								l.getBlock().setType(Material.AIR);
							}
						}
					}
				}
			}
		}, 200L);
	}

}
