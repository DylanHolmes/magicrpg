/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg;

import org.bukkit.Effect;
import org.bukkit.Location;

/**
 *
 * @author simplyianm
 */
public class VisualFX {
	public static void deploySmoke(Location loc, int thickness) {
		for (int i = 0; i < thickness; i++) {
			loc.getWorld().playEffect(loc, Effect.SMOKE, 0, i);
		}
	}

	public static void deployFlames(Location loc, int duration, int width) {
		loc.getWorld().playEffect(loc, Effect.MOBSPAWNER_FLAMES, duration, width);
	}

}