/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.crimsonrpg.magicrpg;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.crimsonrpg.magicrpg.spell.Spell;
import com.crimsonrpg.magicrpg.spell.SpellBlink;
import com.crimsonrpg.magicrpg.spell.SpellDoubleJump;
import com.crimsonrpg.magicrpg.spell.SpellEnderDragon;
import com.crimsonrpg.magicrpg.spell.SpellEnderman;
import com.crimsonrpg.magicrpg.spell.SpellExplosion;
import com.crimsonrpg.magicrpg.spell.SpellFireBall;
import com.crimsonrpg.magicrpg.spell.SpellHeal;
import com.crimsonrpg.magicrpg.spell.SpellLightning;
import com.crimsonrpg.magicrpg.spell.SpellThunderStorm;
import com.crimsonrpg.magicrpg.spell.SpellTimeTravel;
import com.crimsonrpg.magicrpg.spell.SpellTornado;
import com.crimsonrpg.magicrpg.spell.SpellTsunami;
import com.crimsonrpg.magicrpg.spell.SpellWildfire;
import java.util.HashMap;
import org.bukkit.entity.Player;

/**
 *
 * @author simplyianm
 */
public class MagicRPG extends JavaPlugin {

    public static final Logger LOGGER = Logger.getLogger("Minecraft");
    private Map<String, Spell> spells = new HashMap<String, Spell>();

    public void onDisable() {
        LOGGER.log(Level.INFO, "[MagicRPG] Plugin disabled!");
    }

    public void onEnable() {
        registerSpells(
                new SpellBlink(),
                new SpellDoubleJump(),
                new SpellEnderman(this),
                new SpellEnderDragon(this),
                new SpellExplosion(),
                new SpellHeal(),
                new SpellLightning(),
                new SpellTimeTravel(),
                new SpellTornado(this),
                new SpellTsunami(),
                new SpellWildfire(),
                new SpellThunderStorm(this),
                new SpellFireBall(this));


        LOGGER.log(Level.INFO, "[MagicRPG] Plugin enabled!");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You can only use this command in-game.");
            return false;
        }

        if (args.length < 1) {
            sender.sendMessage("You need to input a skill name.");
            return false;
        }

        Spell spell = spells.get(args[0]);
        if (spell == null) {
            sender.sendMessage("There is no spell registered with that name.");
            return false;
        }

        Player player = (Player) sender;
        spell.cast(player);

        return true;
    }

    public void registerSpell(Spell spell) {
        Class<?> type = spell.getClass();

        //Check annotation
        if (!type.isAnnotationPresent(SpellInfo.class)) {
            LOGGER.log(Level.WARNING, "Spell '" + spell.getClass() + "' does not have a @SpellInfo annotation present; skipping...");
            return;
        }

        SpellInfo info = type.getAnnotation(SpellInfo.class);
        String id = info.id();
        String name = info.name();

        spells.put(id, spell);
    }

    public void registerSpells(Spell... spells) {
        for (Spell spell : spells) {
            registerSpell(spell);
        }
    }
}
