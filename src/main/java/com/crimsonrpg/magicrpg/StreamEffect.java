package com.crimsonrpg.magicrpg;

import java.util.ArrayList;


import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public abstract class StreamEffect implements Runnable {
	private Location startLoc;

	private Location endLoc;

	private ArrayList<StreamData> locationsForProjection;

	private double turningPoint;

	private long speedDelay;

	private World world;

	private int i;

	private int id;

	/**
	 * Creates a smoke stream between two points, with several options
	 * @param loc1
	 * starting location
	 * @param loc2
	 * ending location
	 * @param turningPoint
	 * point at which you can choose to have the stream be manipulated
	 */
	public StreamEffect(Location loc1, Location loc2, double turningPoint, long speedDelay) {
		this.startLoc = loc1;
		this.endLoc = loc2;
		this.turningPoint = turningPoint;
		this.world = startLoc.getWorld();
		this.speedDelay = speedDelay;

		locationsForProjection = calculateLocsForProjection();
		this.i = 0;
	}

	public void run() {
		if (i > locationsForProjection.size() - 1) {
			Bukkit.getScheduler().cancelTask(id);
			return;
		}
		StreamData data = locationsForProjection.get(i);
		if (!data.isPastTurningPoint()) {
			actionBeforeTurningPoint(data.getLocation());
			i++;
		} else {
			actionPastTurningPoint(data.getLocation());
			i++;
		}
		if (i == locationsForProjection.size() - 1) {
			actionAtFinalFrame(data.getLocation());
		}

	}

	protected abstract void actionBeforeTurningPoint(Location dataLoc);

	protected abstract void actionPastTurningPoint(Location dataLoc);

	protected abstract void actionAtFinalFrame(Location dataLoc);

	private ArrayList<StreamData> calculateLocsForProjection() {
		double x1, y1, z1, x2, y2, z2, xVect, yVect, zVect;
		x1 = endLoc.getX();
		y1 = endLoc.getY();
		z1 = endLoc.getZ();
		x2 = startLoc.getX();
		y2 = startLoc.getY();
		z2 = startLoc.getZ();
		xVect = x2 - x1;
		yVect = y2 - y1;
		zVect = z2 - z1;
		double distance = startLoc.distance(endLoc);
		ArrayList<StreamData> tmp = new ArrayList<StreamData>((int) Math.floor(distance));

		for (double t = 0; t <= 1; t += 1 / distance) {
			tmp.add(new StreamData(new Location(world, x2 - (xVect * t), y2 - (yVect * t) + 1, z2 - (zVect * t)), t > turningPoint));
		}
		return tmp;
	}

	public void setID(int newID) {
		this.id = newID;
	}

	public long getSpeedDelay() {
		return speedDelay;
	}

}